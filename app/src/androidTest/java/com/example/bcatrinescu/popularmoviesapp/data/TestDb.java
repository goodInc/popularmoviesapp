package com.example.bcatrinescu.popularmoviesapp.data;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;
import android.util.Log;

import java.util.HashSet;

public class TestDb extends AndroidTestCase {

    public static final String LOG_TAG = TestDb.class.getSimpleName();

    // Since we want each test to start with a clean slate
    void deleteTheDatabase() {
        mContext.deleteDatabase(MovieDbHelper.DATABASE_NAME);
    }

    // This function gets called before each test is executed to delete the database.  This makes
    // sure that we always have a clean test.
    public void setUp() {
        deleteTheDatabase();
    }

    public void testCreateDb() throws Throwable {
        // build a HashSet of all of the table names we wish to look for
        // Note that there will be another table in the DB that stores the
        // Android metadata (db version information)
        final HashSet<String> tableNameHashSet = new HashSet<String>();
        tableNameHashSet.add(MovieContract.MovieEntry.TABLE_NAME);
        tableNameHashSet.add(MovieContract.VideosEntry.TABLE_NAME);

        mContext.deleteDatabase(MovieDbHelper.DATABASE_NAME);
        SQLiteDatabase db = new MovieDbHelper(this.mContext).getWritableDatabase();
        assertEquals(true, db.isOpen());

        // have we created the tables we want?
        Cursor c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
        assertTrue("Error: This means that the database has not been created correctly", c.moveToFirst());

        // verify that the tables have been created
        do {
            tableNameHashSet.remove(c.getString(0));
        } while( c.moveToNext() );

        // if this fails, it means that your database doesn't contain both the location entry
        // and weather entry tables
        assertTrue("Error: Your database was created without both the movie entry and video entry tables", tableNameHashSet.isEmpty());

        // now, do our tables contain the correct columns?
        c = db.rawQuery("PRAGMA table_info(" + MovieContract.MovieEntry.TABLE_NAME + ")", null);
        assertTrue("Error: This means that we were unable to query the database for table information.", c.moveToFirst());

        // Build a HashSet of all of the column names we want to look for
        final HashSet<String> movieColumnHashSet = new HashSet<String>();
        movieColumnHashSet.add(MovieContract.MovieEntry._ID);
        movieColumnHashSet.add(MovieContract.MovieEntry.COLUMN_ORIGINAL_TITLE);
        movieColumnHashSet.add(MovieContract.MovieEntry.COLUMN_POSTER_PATH);
        movieColumnHashSet.add(MovieContract.MovieEntry.COLUMN_OVERVIEW);
        movieColumnHashSet.add(MovieContract.MovieEntry.COLUMN_VOTE_AVERAGE);
        movieColumnHashSet.add(MovieContract.MovieEntry.COLUMN_VOTE_COUNT);
        movieColumnHashSet.add(MovieContract.MovieEntry.COLUMN_POPULARITY);
        movieColumnHashSet.add(MovieContract.MovieEntry.COLUMN_RELEASE_DATE);

        Log.v(LOG_TAG, "Size of column hash set is: " + movieColumnHashSet.size());
        int columnNameIndex = c.getColumnIndex("name");
        do {
            String columnName = c.getString(columnNameIndex);
            Log.v(LOG_TAG, "Column name is: " + columnName);
            movieColumnHashSet.remove(columnName);
        } while(c.moveToNext());

        // if this fails, it means that your database doesn't contain all of the required movie entry columns
        assertTrue("Error: The database doesn't contain all of the required movie entry columns", movieColumnHashSet.isEmpty());
        c.close();

        // now, do our tables contain the correct columns?
        c = db.rawQuery("PRAGMA table_info(" + MovieContract.VideosEntry.TABLE_NAME + ")", null);
        assertTrue("Error: This means that we were unable to query the database for table information.", c.moveToFirst());

        // Build a HashSet of all of the column names we want to look for
        final HashSet<String> videoColumnHashSet = new HashSet<String>();
        videoColumnHashSet.add(MovieContract.VideosEntry._ID);
        videoColumnHashSet.add(MovieContract.VideosEntry.COLUMN_MOVIE_ID);
        videoColumnHashSet.add(MovieContract.VideosEntry.COLUMN_TYPE);
        videoColumnHashSet.add(MovieContract.VideosEntry.COLUMN_NAME);
        videoColumnHashSet.add(MovieContract.VideosEntry.COLUMN_LANGUAGE);
        videoColumnHashSet.add(MovieContract.VideosEntry.COLUMN_SITE);
        videoColumnHashSet.add(MovieContract.VideosEntry.COLUMN_KEY);
        videoColumnHashSet.add(MovieContract.VideosEntry.COLUMN_RES_SIZE);

        Log.v(LOG_TAG, "Size of column hash set is: " + videoColumnHashSet.size());
        columnNameIndex = c.getColumnIndex("name");
        do {
            String columnName = c.getString(columnNameIndex);
            Log.v(LOG_TAG, "Column name is: " + columnName);
            videoColumnHashSet.remove(columnName);
        } while(c.moveToNext());

        // if this fails, it means that your database doesn't contain all of the required movie entry columns
        assertTrue("Error: The database doesn't contain all of the required video entry columns", videoColumnHashSet.isEmpty());
        c.close();
        db.close();
    }
//
//    public void testMovieTable() {
//       insertLocation();
//    }
//
//    /*
//        Students:  Here is where you will build code to test that we can insert and query the
//        database.  We've done a lot of work for you.  You'll want to look in TestUtilities
//        where you can use the "createWeatherValues" function.  You can
//        also make use of the validateCurrentRecord function from within TestUtilities.
//     */
//    public void testWeatherTable() {
//        // First insert the location, and then use the locationRowId to insert
//        // the weather. Make sure to cover as many failure cases as you can.
//
//        // Instead of rewriting all of the code we've already written in testLocationTable
//        // we can move this code to insertLocation and then call insertLocation from both
//        // tests. Why move it? We need the code to return the ID of the inserted location
//        // and our testLocationTable can only return void because it's a test.
//
//        long locationRowId = insertLocation();
//
//        // First step: Get reference to writable database
//        SQLiteDatabase weatherDatabase = new WeatherDbHelper(mContext).getWritableDatabase();
//
//        // Create ContentValues of what you want to insert
//        // (you can use the createWeatherValues TestUtilities function if you wish)
//        ContentValues contentValues = TestUtilities.createWeatherValues(Long.parseLong(TestUtilities.TEST_LOCATION));
//
//        // Insert ContentValues into database and get a row ID back
//        long weatherRowId = weatherDatabase.insert(WeatherContract.WeatherEntry.TABLE_NAME, null, contentValues);
//        assertTrue("Can't insert data into the Weather table", weatherRowId != -1);
//
//        // Query the database and receive a Cursor back
//        Cursor cursor = weatherDatabase.query(WeatherContract.WeatherEntry.TABLE_NAME, null, null, null, null, null, null);
//
//        // Move the cursor to a valid database row
//        assertTrue("The Cursor that interrogated the Weather table is empty. Did you wrote the query correctly?", cursor.moveToFirst());
//
//        // Validate data in resulting Cursor with the original ContentValues
//        // (you can use the validateCurrentRecord function in TestUtilities to validate the
//        // query if you like)
//        TestUtilities.validateCursor("The weather data doesn't seem to be the one I expected", cursor, contentValues);
//
//        assertFalse("The table contains more than 1 row... it shouldn't!", cursor.moveToNext());
//
//        // Finally, close the cursor and database
//        weatherDatabase.close();
//        cursor.close();
//    }
//
//
//    /*
//        Students: This is a helper method for the testWeatherTable quiz. You can move your
//        code from testLocationTable to here so that you can call this code from both
//        testWeatherTable and testLocationTable.
//     */
//    public long insertLocation() {
//        // First step: Get reference to writable database
//        SQLiteDatabase locationTableDB = new WeatherDbHelper(mContext).getWritableDatabase();
//
//        // Create ContentValues of what you want to insert
//        // (you can use the createNorthPoleLocationValues if you wish)
//        ContentValues contentValues = TestUtilities.createNorthPoleLocationValues();
//
//        // Insert ContentValues into database and get a row ID back
//        long insertedRowID = locationTableDB.insert(WeatherContract.LocationEntry.TABLE_NAME,null,contentValues);
//        assertTrue("The row could not be created in the location table", insertedRowID != -1);
//
//        // Query the database and receive a Cursor back
//        Cursor cursor = locationTableDB.query(WeatherContract.LocationEntry.TABLE_NAME,
//                new String[]{WeatherContract.LocationEntry.COLUMN_LOCATION_SETTING,
//                        WeatherContract.LocationEntry.COLUMN_CITY_NAME,
//                        WeatherContract.LocationEntry.COLUMN_COORD_LAT,
//                        WeatherContract.LocationEntry.COLUMN_COORD_LONG},
//                null,null,null, null, null
//        );
//
//        // Move the cursor to a valid database row
//        assertTrue("It seems that the query did not return any result!",cursor.moveToFirst() == true);
//
//        // Validate data in resulting Cursor with the original ContentValues
//        // (you can use the validateCurrentRecord function in TestUtilities to validate the
//        // query if you like)
//        TestUtilities.validateCursor("Error: Location Query Validation Failed", cursor, contentValues);
//
//        // Confirm that the database table has only 1 row
//        assertFalse(cursor.moveToNext());
//
//        // Finally, close the cursor and database
//        locationTableDB.close();
//        cursor.close();
//        return insertedRowID;
//    }
}
