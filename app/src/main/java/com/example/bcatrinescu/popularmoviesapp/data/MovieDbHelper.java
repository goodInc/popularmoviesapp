package com.example.bcatrinescu.popularmoviesapp.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.bcatrinescu.popularmoviesapp.data.MovieContract.MovieEntry;
import com.example.bcatrinescu.popularmoviesapp.data.MovieContract.VideosEntry;
import com.example.bcatrinescu.popularmoviesapp.data.MovieContract.FavoriteEntry;

/**
 * Created by bcatrinescu on 28.10.2016.
 */

public class MovieDbHelper extends SQLiteOpenHelper {
    private static final String LOG_TAG = MovieDbHelper.class.getSimpleName();
    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 10;
    // set the db name
    static final String DATABASE_NAME = "movie.db";

    public MovieDbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        final String SQL_CREATE_MOVIE_TABLE = "CREATE TABLE " +
                MovieEntry.TABLE_NAME + " (" +
                MovieEntry._ID + " INTEGER PRIMARY KEY ON CONFLICT REPLACE, " +
                MovieEntry.COLUMN_ORIGINAL_TITLE + " TEXT NOT NULL, " +
                MovieEntry.COLUMN_POSTER_PATH + " TEXT NOT NULL, " +
                MovieEntry.COLUMN_OVERVIEW + " TEXT NOT NULL, " +
                MovieEntry.COLUMN_VOTE_AVERAGE + " REAL NOT NULL, " +
                MovieEntry.COLUMN_VOTE_COUNT + " INTEGER NOT NULL, " +
                MovieEntry.COLUMN_POPULARITY + " REAL NOT NULL, " +
                MovieEntry.COLUMN_RELEASE_DATE + " INTEGER NOT NULL " +
                ");";
        final String SQL_CREATE_VIDEO_TABLE = "CREATE TABLE " +
                VideosEntry.TABLE_NAME + " (" +
                VideosEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                VideosEntry.COLUMN_MOVIE_ID + " INTEGER NOT NULL, " +
                VideosEntry.COLUMN_TYPE + " TEXT NOT NULL, " +
                VideosEntry.COLUMN_NAME + " TEXT NOT NULL, " +
                VideosEntry.COLUMN_LANGUAGE + " TEXT NOT NULL, " +
                VideosEntry.COLUMN_SITE + " TEXT NOT NULL, " +
                VideosEntry.COLUMN_KEY + " TEXT NOT NULL, " +
                VideosEntry.COLUMN_RES_SIZE + " INTEGER NOT NULL, " +
                "FOREIGN KEY " + "(" + VideosEntry.COLUMN_MOVIE_ID + ")" + " REFERENCES " +
                MovieEntry.TABLE_NAME + " (" + MovieEntry._ID + ")" +
                ");";
        final String SQL_CREATE_FAVORITE_TABLE = "CREATE TABLE " +
                FavoriteEntry.TABLE_NAME + " (" +
                FavoriteEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                FavoriteEntry.COLUMN_FAVORITE_MOVIE_ID + " INTEGER NOT NULL, " +
                "FOREIGN KEY " + "(" + FavoriteEntry.COLUMN_FAVORITE_MOVIE_ID + ")" + " REFERENCES " +
                MovieEntry.TABLE_NAME + " (" + MovieEntry._ID + ")" +
                ");";
        sqLiteDatabase.execSQL(SQL_CREATE_MOVIE_TABLE);
//        Log.v(LOG_TAG, "Query movie:\n" + SQL_CREATE_MOVIE_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_VIDEO_TABLE);
//        Log.v(LOG_TAG, "Query video:\n" + SQL_CREATE_VIDEO_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_FAVORITE_TABLE);
//        Log.v(LOG_TAG, "Query favorite:\n" + SQL_CREATE_FAVORITE_TABLE);
        Log.v(LOG_TAG, "DATABASE tables created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        Log.w(LOG_TAG, "DELETING tables, for now, on database upgrade. You should NOT do this in production!!");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + MovieEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + VideosEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + FavoriteEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
