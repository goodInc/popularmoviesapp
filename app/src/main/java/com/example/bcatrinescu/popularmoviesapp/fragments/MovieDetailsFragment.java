package com.example.bcatrinescu.popularmoviesapp.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.bcatrinescu.popularmoviesapp.BuildConfig;
import com.example.bcatrinescu.popularmoviesapp.R;
import com.example.bcatrinescu.popularmoviesapp.data.MovieContract;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.Map;

public class MovieDetailsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    public static final String MOVIEDETAILFRAGMENT_TAG = "MDFTAG";
    private static final String LOG_TAG = MovieDetailsFragment.class.getSimpleName();
    private static final int MOVIE_DETAIL_LOADER = 122511;
    private final String POSTERSIZELARGE = "w342";
    private final String MOVIE_BASE_URL = "http://image.tmdb.org/t/p";
    private Uri mUri;
    private ViewHolder sViewHolder;

    public static final String[] MOVIE_DETAILS_COLUMNS = {
            MovieContract.MovieEntry.TABLE_NAME + "." + MovieContract.MovieEntry._ID,
            MovieContract.MovieEntry.COLUMN_ORIGINAL_TITLE,
            MovieContract.MovieEntry.COLUMN_POSTER_PATH,
            MovieContract.MovieEntry.COLUMN_OVERVIEW,
            MovieContract.MovieEntry.COLUMN_VOTE_AVERAGE,
            MovieContract.MovieEntry.COLUMN_VOTE_COUNT,
            MovieContract.MovieEntry.COLUMN_POPULARITY,
            MovieContract.MovieEntry.COLUMN_RELEASE_DATE,
            MovieContract.FavoriteEntry.COLUMN_FAVORITE_MOVIE_ID
    };
    // These indices are tied to the MOVIE_DETAILS_COLUMNS. If MOVIE_DETAILS_COLUMNS changes, the values below MUST change
    public static final int COL_MOVIE_ID = 0;
    public static final int COL_ORIGINAL_TITLE = 1;
    public static final int COL_POSTER_PATH = 2;
    public static final int COL_OVERVIEW = 3;
    public static final int COL_VOTE_AVERAGE = 4;
    public static final int COL_VOTE_COUNT = 5;
    public static final int COL_POPULARITY = 6;
    public static final int COL_RELEASE_DATE = 7;
    public static final int COL_FAV_MOVIE_ID = 8;

    public static MovieDetailsFragment getInstanceByUri(Uri uri) {
        MovieDetailsFragment movieDetailsFragment = null;
        if (uri != null) {
            movieDetailsFragment = new MovieDetailsFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable(MOVIEDETAILFRAGMENT_TAG, uri);
            movieDetailsFragment.setArguments(bundle);
        } else {
            Log.w(LOG_TAG, "The Uri passed by the parent activity is NULL. I can't create a MovieDetailsFragment instance");
        }
        return movieDetailsFragment;
    }
    public static MovieDetailsFragment getInstanceById(long movieId) {
        MovieDetailsFragment movieDetailsFragment = null;
        if (movieId > 0) {
            movieDetailsFragment = new MovieDetailsFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable(MOVIEDETAILFRAGMENT_TAG, MovieContract.MovieEntry.buildMovieUri(movieId));
            movieDetailsFragment.setArguments(bundle);
        } else {
            Log.w(LOG_TAG, "The movie ID passed by the parent activity is NULL. I can't create a MovieDetailsFragment instance");
        }
        return movieDetailsFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mUri = arguments.getParcelable(MovieDetailsFragment.MOVIEDETAILFRAGMENT_TAG);
//            Log.v(LOG_TAG, "from Parcelable: " + mUri.toString());
        }
        View view = inflater.inflate(R.layout.fragment_movie_details, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        getLoaderManager().initLoader(MOVIE_DETAIL_LOADER, null, this);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(
                getContext(),
                mUri, // the Uri will be created and send this from the MoviesActivity callback
                MOVIE_DETAILS_COLUMNS,
                null,
                null,
                null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (!data.moveToFirst()) { Log.v(LOG_TAG, "It seems that the Movie Details cursor is empty! Existing without loading anything"); return; }
        /** Get the holder object containing each View id we are interested in, part of our layout */
        sViewHolder = new ViewHolder(getView(),getContext());

        String originalTitle = data.getString(COL_ORIGINAL_TITLE);
        (sViewHolder.originalTitle).setText(originalTitle);

        String userRating = data.getString(COL_VOTE_AVERAGE);
        (sViewHolder.userRating).setText(userRating);

        String releaseDate = data.getString(COL_RELEASE_DATE);
        (sViewHolder.releaseDate).setText(releaseDate);

        String overviewContent = data.getString(COL_OVERVIEW);
        (sViewHolder.overviewContent).setText(overviewContent);

        String posterPath = data.getString(COL_POSTER_PATH);
        /** Build the poster Uri */
        Uri posterUri = Uri.parse(MOVIE_BASE_URL).buildUpon().
                appendPath(POSTERSIZELARGE).
                appendPath(posterPath).
                appendQueryParameter("api_key", BuildConfig.THE_MOVIE_DATABASE_API_KEY).
                build();
        // Load the poster as the background of the layout in a separate thread using a callback
        Picasso.with(getContext()).load(posterUri).into(target);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) { /* nothing to do here, as we don't need to refresh the data */}

    /* This is a callback used by Piccaso to load the image into another thread */
    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            DisplayMetrics metrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
            Configuration config = getResources().getConfiguration();
            Resources resources = new Resources(getContext().getAssets(), metrics, config);
            BitmapDrawable bitmapDrawable = new BitmapDrawable(resources, bitmap);
            (sViewHolder.posterBackgroundLinearView).setBackground(bitmapDrawable);
//            Log.v(LOG_TAG, "done loading the bitmap in the background of this layout");
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
            Log.w(LOG_TAG, "Picasso target bitmap loading failed");
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
//            Log.v(LOG_TAG, "placeholder drawable should be set here");
        }
    };

    /** This static object will store the IDs of each of the available views. Use it in a view TAG */
    private static class ViewHolder {
        public final LinearLayout posterBackgroundLinearView;
        public final TextView originalTitle;
        public final TextView userRating;
        public final TextView releaseDate;
        public final TextView overviewContent;

        public ViewHolder(View v, Context context){
            posterBackgroundLinearView = (LinearLayout) v.findViewById(R.id.movie_details_root);
            originalTitle = (TextView) v.findViewById(R.id.movie_original_title);
            userRating = (TextView) v.findViewById(R.id.movie_user_rating);
            releaseDate = (TextView) v.findViewById(R.id.movie_release_date);
            overviewContent = (TextView) v.findViewById(R.id.movie_overview_content);
        }
    }
}
