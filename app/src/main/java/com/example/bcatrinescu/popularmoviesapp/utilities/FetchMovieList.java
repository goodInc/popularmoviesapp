package com.example.bcatrinescu.popularmoviesapp.utilities;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.example.bcatrinescu.popularmoviesapp.BuildConfig;
import com.example.bcatrinescu.popularmoviesapp.adapters.MoviesSimpleAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bcatrinescu on 12.09.2016.
 */
public class FetchMovieList extends AsyncTask<String, Void, List<Map<String,String>>> {
    private static final String LOG_TAG = FetchMovieList.class.getSimpleName();
    private final String MOVIE_BASE_URL = "http://api.themoviedb.org/3/movie";

    private final int URL_CONNECT_TIMEOUT_MILS = 2000;
    private final int URL_READ_TIMEOUT_MILS = 2500;

    @Override
    protected List<Map<String,String>> doInBackground(String... params) {
//        Log.v(LOG_TAG, "Updating movies...");
        /** Create internet connection objects */
        Uri uri = Uri.parse(MOVIE_BASE_URL).buildUpon()
                .appendPath(params[0])
                .appendQueryParameter("page", params[1])
                .appendQueryParameter("api_key", BuildConfig.THE_MOVIE_DATABASE_API_KEY).build();

//        Log.v(LOG_TAG, "The URL to query is: " + uri.toString());
        URL url = null;
        HttpURLConnection httpURLConnection = null;
        BufferedReader bufferedReader = null;
        StringBuilder resultStringBuilder = new StringBuilder();
        String moviesJsonStr = null;

        try {
            url = new URL(uri.toString());
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setReadTimeout(URL_READ_TIMEOUT_MILS);
            httpURLConnection.setConnectTimeout(URL_CONNECT_TIMEOUT_MILS);
            httpURLConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = httpURLConnection.getInputStream();
            // If the inputStream variable is empty, there is nothing to do. No data was fetched
            if (inputStream == null) return null;
            // if there is data in the inputStream, continue making a buffered reader
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                // since this is a JSON, newlines don't really count, but it will make a pretty output
                // useful for debugging
                resultStringBuilder.append(line + "\n");
            }
            if (resultStringBuilder.length() == 0) {
                // result string is empty, no point in continuing
                return null;
            }
            /** If everything was OK, set the final GET result as a JSON */
            moviesJsonStr = resultStringBuilder.toString();
//            Log.v(LOG_TAG, "RAW data: \n" + moviesJsonStr);
            /** Error handling */
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "URL creation from Uri seems to be broken. The Uri is: " + uri.toString());
            e.printStackTrace();
        } catch (IOException e) {
            Log.e(LOG_TAG, "Can't open the HTTP connection (not yet connected). The URL is: " + url.toString());
            e.printStackTrace();
        } finally {
            if (httpURLConnection != null) httpURLConnection.disconnect();
            if (bufferedReader != null) {
                try { bufferedReader.close(); } catch (IOException e) {
                    Log.e(LOG_TAG, "Error closing buffered reader stream", e);
                }
            }
        }

        List<Map<String,String>> listOfMaps = moviesJsonToListOfMaps(moviesJsonStr);

        if (listOfMaps == null) {
            Log.w(LOG_TAG, "The list of movies used to populate our list is NULL (after parsing). Take extra care!");
        }
//        Log.v(LOG_TAG, "ListOfMaps has this number of elements: " + listOfMaps.size());
        return listOfMaps;
    }

    @Override
    protected void onPostExecute(List<Map<String,String>> maps) {
//        Log.v(LOG_TAG, "on post execute... calling update data");
        MoviesSimpleAdapter.updateData(maps);
    }

    private List<Map<String,String>> moviesJsonToListOfMaps(String moviesJsonStr) {
        List<Map<String,String>> moviesList = null;
        try {
            JSONObject moviesJsonObject;
            moviesJsonObject = new JSONObject(moviesJsonStr);
            JSONArray moviesJsonArray = moviesJsonObject.getJSONArray("results");
            JSONObject movieJsonObject = null;
            moviesList = new ArrayList<>();
            int counter = 0;
            while ((movieJsonObject = moviesJsonArray.optJSONObject(counter++)) != null) {
                Map<String,String> movieMap = new HashMap<>();
                String posterPath = movieJsonObject.getString("poster_path").substring(1);
//                Log.v(LOG_TAG, "the original poster path is: " + posterPath);
                String originalTitle = movieJsonObject.getString("original_title");
                String overview = movieJsonObject.getString("overview");
                String userRating = movieJsonObject.getString("vote_average");
                String releaseDate = movieJsonObject.getString("release_date");
                movieMap.put("posterPath",posterPath);
                movieMap.put("originalTitle", originalTitle);
                movieMap.put("overview", overview);
                movieMap.put("userRating", userRating);
                movieMap.put("releaseDate", releaseDate);
                moviesList.add(movieMap);
            }

        } catch (JSONException e) {
            Log.e(LOG_TAG, "The movie JSON String can't be parsed into an usable object!", e);
        }
        return moviesList;
    }
}
