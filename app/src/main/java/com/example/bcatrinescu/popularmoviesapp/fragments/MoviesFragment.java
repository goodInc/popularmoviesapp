package com.example.bcatrinescu.popularmoviesapp.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.example.bcatrinescu.popularmoviesapp.MovieDetailsActivity;
import com.example.bcatrinescu.popularmoviesapp.R;
import com.example.bcatrinescu.popularmoviesapp.adapters.MoviesCursorAdapter;
import com.example.bcatrinescu.popularmoviesapp.data.MovieContract;
import com.example.bcatrinescu.popularmoviesapp.sync.MovieSyncAdapter;

import java.util.Map;

public class MoviesFragment extends Fragment implements SharedPreferences.OnSharedPreferenceChangeListener, LoaderManager.LoaderCallbacks<Cursor> {
    private static final String LOG_TAG = MoviesFragment.class.getSimpleName();
    private static final int MOVIE_LOADER = 231124;
    private MoviesCursorAdapter mMoviesCursorAdapter;
    private SharedPreferences sharedPreferences;
    private String currentSortOrder;
    private String lastSortOrder = "";

    public static final String[] MOVIE_LIST_COLUMNS = {
            // In this case the id needs to be fully qualified with a table name, since the content provider
            // joins the 'movie', 'video' and 'favorite' tables in the background (all have an _id column)
            MovieContract.MovieEntry.TABLE_NAME + "." + MovieContract.MovieEntry._ID,
            MovieContract.MovieEntry.COLUMN_ORIGINAL_TITLE,
            MovieContract.MovieEntry.COLUMN_POSTER_PATH,
            MovieContract.MovieEntry.COLUMN_VOTE_AVERAGE,
            MovieContract.FavoriteEntry.COLUMN_FAVORITE_MOVIE_ID
    };

    // These indices are tied to the MOVIE_LIST_COLUMNS. If MOVIE_LIST_COLUMNS changes, the values below MUST change
    public static final int COL_MOVIE_ID = 0;
    public static final int COL_ORIGINAL_TITLE = 1;
    public static final int COL_POSTER_PATH = 2;
    public static final int COL_VOTE_AVERAGE = 3;
    public static final int COL_FAV_MOVIE_ID = 4;

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        if (currentSortOrder.compareTo(lastSortOrder) != 0) {
            lastSortOrder = currentSortOrder;
            updateMovies();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        currentSortOrder = sharedPreferences.getString("sortOrder", "popular");
        getLoaderManager().initLoader(MOVIE_LOADER, null, this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        /** we should notify the activity that we have a menu as the LAST thing to do OR, at least,
         *  after getting the shared preferences values */
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.movies_list,menu);

        MenuItem item = menu.findItem(R.id.spinner_sort_movies_by);
        Spinner spinner = (Spinner) MenuItemCompat.getActionView(item);
        /** create an adapter for this spinner. we'll use it to populate it with static defined items */
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(getContext(),R.array.sort_types_array, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(spinnerAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0: currentSortOrder = "popular"; break;
                    case 1: currentSortOrder = "top_rated"; break;
                    case 2: currentSortOrder = "favorite"; break;
                    default:
                        currentSortOrder = "popular";
                        Log.w(LOG_TAG, "Switching to POPULAR, as unsupported option in spinner - on item selected (defining listeners): " + currentSortOrder);
                }
                sharedPreferences.edit().putString("sortOrder", currentSortOrder).apply();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        switch (currentSortOrder) {
            case "popular":
                spinner.setSelection(0);
                break;
            case "top_rated":
                spinner.setSelection(1);
                break;
            case "favorite":
                spinner.setSelection(2);
                break;
            default:
                spinner.setSelection(0);
                currentSortOrder = "popular";
                Log.w(LOG_TAG, "Switching to POPULAR, as unsupported option in spinner - on item selected: " + currentSortOrder);
        }
    }

///*
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        if (id == R.id.spinner_sort_movies_by) {
//            currentSortOrder = (currentSortOrder.compareTo("popular") == 0) ? "top_rated" : "popular";
//            if (currentSortOrder.compareTo("popular") == 0)
//                item.setTitle(R.string.sort_movies_by_popular);
//            else
//                item.setTitle(R.string.sort_movies_by_top_rated);
////            Toast.makeText(getContext(), "Click!\n" + "curr: " + currentSortOrder + "\nlast: " + lastSortOrder, Toast.LENGTH_SHORT).show();
//            sharedPreferences.edit().putString("sortOrder", currentSortOrder).commit();
//        }
//        return super.onOptionsItemSelected(item);
//    }
//*/

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_movies, container, false);
        mMoviesCursorAdapter = new MoviesCursorAdapter(getContext(), null, 0);
        final GridView gridView = (GridView) rootView.findViewById(R.id.movies_grid);
        gridView.setAdapter(mMoviesCursorAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Cursor cursor = (Cursor) adapterView.getItemAtPosition(position);
//                Log.v(LOG_TAG, "Click on movie poster! and view class is: " + view.getClass().getSimpleName());
                if (cursor == null) {
                    Log.w(LOG_TAG, "The Cursor for this item seems to be NULL!. The action associated with it won't be done!");
                    return;
                }
                long movieId = cursor.getLong(COL_MOVIE_ID);
                /** Given the fact that we have to create two separate URIs, we will create the second one in the child activity */
                Uri uri = MovieContract.MovieEntry.buildMovieUri(movieId);
                // Now, let others do the work for us :) (the parent activity will call/start the other activity)
                FragmentActivity fragmentActivity = getActivity();
                if (!(fragmentActivity instanceof Callback)) {
                    throw new RuntimeException("The parent activity doesn't implement the Callback interface");
                } else {
                    ((Callback) fragmentActivity).onItemSelected(uri);
                }
            }
        });
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        updateMovies();
    }

    private void updateMovies(){
        //MovieSyncAdapter.syncImmediately(getActivity());
        getLoaderManager().restartLoader(MOVIE_LOADER, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // Now create and return a CursorLoader that will take care of
        // creating a Cursor for the data being displayed.
        currentSortOrder = sharedPreferences.getString("sortOrder", "popular");
        Uri movieContentUri = null;
//        Log.v(LOG_TAG, "the current sort order is: " + currentSortOrder);
        switch (currentSortOrder) {
            case "popular":
                movieContentUri = MovieContract.MovieEntry.buildPopularMovieUri();
                break;
            case "top_rated":
                movieContentUri = MovieContract.MovieEntry.buildRatingMovieUri();
                break;
            case "favorite":
                movieContentUri = MovieContract.MovieEntry.buildFavoriteMovieUri();
                break;
            default:
                currentSortOrder = "popular";
                movieContentUri = MovieContract.MovieEntry.buildPopularMovieUri();
                Log.w(LOG_TAG, "Switching to POPULAR, as Unsupported 'current sort order' while trying to generate the URI for the CursorLoader: " + currentSortOrder);
        }
        return new CursorLoader(
                getContext(),
                movieContentUri,
                MOVIE_LIST_COLUMNS,
                null,
                null,
                null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mMoviesCursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mMoviesCursorAdapter.swapCursor(null);
    }

    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface Callback {
        /**
         * Callback for when an item has been selected.
         */
        public void onItemSelected(Uri movieUri);
    }
}
