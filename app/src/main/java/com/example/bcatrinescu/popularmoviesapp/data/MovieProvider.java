package com.example.bcatrinescu.popularmoviesapp.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by bcatrinescu on 28.10.2016.
 */

public class MovieProvider extends ContentProvider {
    private static final String LOG_TAG = MovieProvider.class.getSimpleName();
    // The Uri Matcher used by this content provider
    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private static final int MOVIE = 100;
    private static final int MOVIE_WITH_ID = 101;
    private static final int MOVIE_WITH_ID_VIDEOS = 102;
    private static final int MOVIE_LIST_POPULAR = 201;
    private static final int MOVIE_LIST_RATING = 202;
    private static final int MOVIE_LIST_FAVORITE = 203;
    private static final int VIDEO = 300;
    private static final int FAVORITE = 400;

    private MovieDbHelper mOpenHelper;
    // the queryBuilder will be used when a join between the Movie and the Video tables is required
    private static final SQLiteQueryBuilder sMovieFavoriteQueryBuilder;
    private static final SQLiteQueryBuilder sVideoFavoriteQueryBuilder;
    static {
        sMovieFavoriteQueryBuilder = new SQLiteQueryBuilder();
        // Left JOIN between the movie and the favorite tables
        sMovieFavoriteQueryBuilder.setTables(MovieContract.MovieEntry.TABLE_NAME +
                " Left JOIN " + MovieContract.FavoriteEntry.TABLE_NAME +
                " ON " + MovieContract.MovieEntry.TABLE_NAME + "." + MovieContract.MovieEntry._ID +
                " = " + MovieContract.FavoriteEntry.TABLE_NAME + "." + MovieContract.FavoriteEntry.COLUMN_FAVORITE_MOVIE_ID);
//        Log.v(LOG_TAG, "movie-favorite query is:\n" + sMovieFavoriteQueryBuilder.getTables());
    }
    static {
        sVideoFavoriteQueryBuilder = new SQLiteQueryBuilder();
        // Inner JOIN between the movie, the video and the favorite, tables
        sVideoFavoriteQueryBuilder.setTables(MovieContract.VideosEntry.TABLE_NAME +
                " Left JOIN " + MovieContract.FavoriteEntry.TABLE_NAME +
                " ON " + MovieContract.VideosEntry.TABLE_NAME + "." + MovieContract.VideosEntry.COLUMN_MOVIE_ID +
                " = " + MovieContract.FavoriteEntry.TABLE_NAME + "." + MovieContract.FavoriteEntry.COLUMN_FAVORITE_MOVIE_ID);
//        Log.v(LOG_TAG, "video-favorite query is:\n" + sVideoFavoriteQueryBuilder.getTables());
    }
    // this looks like movie._id = ?; it will be used to retrieve only one movie, by it's id;
    private static final String sMovieSelection = MovieContract.MovieEntry.TABLE_NAME + "." + MovieContract.MovieEntry._ID + " = ? ";
    private static final String sVideoSelection = MovieContract.VideosEntry.TABLE_NAME + "." + MovieContract.VideosEntry.COLUMN_MOVIE_ID + " = ? ";
    private static final String sMovieListFavorites = MovieContract.FavoriteEntry.TABLE_NAME + "." + MovieContract.FavoriteEntry.COLUMN_FAVORITE_MOVIE_ID + " IS NOT NULL ";

    @Override
    public boolean onCreate() {
        mOpenHelper = new MovieDbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        // Here's the switch statement that, given a URI, will determine what kind of request it is,
        // and query the database accordingly.
        Cursor retCursor;
        switch (sUriMatcher.match(uri)){
            // "movie/#"
            case MOVIE_WITH_ID: {
                retCursor = getMovieWithId(uri, projection, sortOrder);
//                Log.v(LOG_TAG, "query... Match 'movie with id': " + uri);
                break;
            }
            // "movie/#/videos"
            case MOVIE_WITH_ID_VIDEOS: {
                retCursor = getMovieWithIdVideos(uri, projection, sortOrder);
//                Log.v(LOG_TAG, "query... Match 'movie with id Videos': " + uri);
                break;
            }
            case MOVIE_LIST_POPULAR: {
                retCursor = getMoviePopular(uri, projection, sortOrder);
//                Log.v(LOG_TAG, "query... Match 'all movies by popularity': " + uri);
                break;
            }
            case MOVIE_LIST_RATING: {
                retCursor = getMovieRating(uri, projection, sortOrder);
//                Log.v(LOG_TAG, "query... Match 'all movies by rating': " + uri);
                break;
            }
            case MOVIE_LIST_FAVORITE: {
                retCursor = getMovieFavorites(uri, projection, sortOrder);
//                Log.v(LOG_TAG, "query... Match 'movie favorite': " + uri);
                break;
            }
            // /movie
            // basic query that will get you all movies in the table (useful for testing) or inserting
            case MOVIE: {
                retCursor = mOpenHelper.getReadableDatabase().query(
                        MovieContract.MovieEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
//                Log.v(LOG_TAG, "query... Match 'all movies': " + uri);
                break;
            }
            // /video
            // basic query that will get you all videos in the table (useful for testing) or inserting
            case VIDEO: {
                retCursor = mOpenHelper.getReadableDatabase().query(
                        MovieContract.VideosEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
//                Log.v(LOG_TAG, "query... Match 'all videos': " + uri);
                break;
            }
            // /favorite
            // basic query that will get you all favorite movies in the table (useful for testing) or inserting
            case FAVORITE: {
//                Log.v(LOG_TAG, "Matched 'Favorite' and that is it");
                retCursor = mOpenHelper.getReadableDatabase().query(
                        MovieContract.FavoriteEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
//                Log.v(LOG_TAG, "query... Match 'all favorite movies': " + uri);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
//        Log.v(LOG_TAG, "SET Notification URI as: " + uri);
        return retCursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        // Use the Uri Matcher to determine what kind of URI this is.

//        Log.v(LOG_TAG, "Uri to match is: " +  uri);
        switch (sUriMatcher.match(uri)) {
            case MOVIE:
                return MovieContract.MovieEntry.CONTENT_TYPE;
            case MOVIE_WITH_ID:
                return MovieContract.MovieEntry.CONTENT_ITEM_TYPE;
            case MOVIE_WITH_ID_VIDEOS:
                return MovieContract.MovieEntry.CONTENT_TYPE;
            case MOVIE_LIST_POPULAR:
                return MovieContract.MovieEntry.CONTENT_TYPE;
            case MOVIE_LIST_RATING:
                return MovieContract.MovieEntry.CONTENT_TYPE;
            case MOVIE_LIST_FAVORITE:
                return MovieContract.MovieEntry.CONTENT_TYPE;
            case VIDEO:
                return MovieContract.VideosEntry.CONTENT_TYPE;
            case FAVORITE:
                return MovieContract.FavoriteEntry.CONTENT_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri returnUri;

        switch (match) {
            case MOVIE: {
                long _id = db.insert(MovieContract.MovieEntry.TABLE_NAME, null, values);
                if (_id > 0) {
                    returnUri = MovieContract.MovieEntry.buildMovieUri(_id);
                } else {
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                }
                break;
            }
            case VIDEO: {
                long _id = db.insert(MovieContract.VideosEntry.TABLE_NAME, null, values);
                if (_id > 0) {
                    returnUri = MovieContract.VideosEntry.buildVideoUri(_id);
                } else {
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                }
                break;
            }
            case FAVORITE: {
                long _id = db.insert(MovieContract.FavoriteEntry.TABLE_NAME, null, values);
                if (_id > 0) {
                    returnUri = MovieContract.FavoriteEntry.buildFavoriteUri(_id);
                } else {
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                }
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
//        Log.v(LOG_TAG, "Insert single URI. DONE! Notifying the following URI of the change: " + uri.toString());
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int rowsDeleted = -1;
        switch (sUriMatcher.match(uri)) {
            case MOVIE: {
                rowsDeleted = db.delete(MovieContract.MovieEntry.TABLE_NAME, selection, selectionArgs);
                break;
            }
            case VIDEO: {
                rowsDeleted = db.delete(MovieContract.VideosEntry.TABLE_NAME, selection, selectionArgs);
                break;
            }
            case FAVORITE: {
                rowsDeleted = db.delete(MovieContract.FavoriteEntry.TABLE_NAME, selection, selectionArgs);
                break;
            }
            default:
                throw new UnsupportedOperationException("Operation (delete) unsupported for Uri: " + uri);
        }
        if (rowsDeleted != 0 ) getContext().getContentResolver().notifyChange(uri,null);
//        Log.v(LOG_TAG, String.format("A number of %d rows were deleted; uri was: %s", rowsDeleted, uri.toString()));
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int rowsUpdated = -1;
        switch (sUriMatcher.match(uri)) {
            case MOVIE: {
                rowsUpdated = db.update(MovieContract.MovieEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            }
            case VIDEO: {
                rowsUpdated = db.update(MovieContract.VideosEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            }
            case FAVORITE: {
                rowsUpdated = db.update(MovieContract.FavoriteEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            }
            default:
                throw new UnsupportedOperationException("Update operation unknown for Uri: " + uri);
        }
        if (rowsUpdated != 0) getContext().getContentResolver().notifyChange(uri, null);
//        Log.v(LOG_TAG, String.format("Update operation done for %d rows with Uri: %s", rowsUpdated, uri));
        return rowsUpdated;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case MOVIE: {
                db.beginTransaction();
                int returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insertWithOnConflict(MovieContract.MovieEntry.TABLE_NAME, null, value, SQLiteDatabase.CONFLICT_REPLACE);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
//                Log.v(LOG_TAG, String.format("Movies BULK INSERT; %d records put in the Popular Movies DB and URI is: %s", returnCount, uri));
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            }
            case VIDEO: {
                db.beginTransaction();
                int returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insertWithOnConflict(MovieContract.VideosEntry.TABLE_NAME, null, value, SQLiteDatabase.CONFLICT_REPLACE);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
//                Log.v(LOG_TAG, String.format("Videos BULK INSERT; %d records put in the Popular Movies DB and URI is: %s", returnCount, uri));
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            }
            default:
                return super.bulkInsert(uri, values);
        }
    }

    static UriMatcher buildUriMatcher() {
        // 1) The code passed into the constructor represents the code to return for the root
        // URI.  It's common to use NO_MATCH as the code for this case. Add the constructor below.

        UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

        // 2) Use the addURI function to match each of the types.  Use the constants from
        // WeatherContract to help define the types to the UriMatcher.
        uriMatcher.addURI(MovieContract.CONTENT_AUTHORITY, MovieContract.PATH_MOVIE, MOVIE);
        uriMatcher.addURI(MovieContract.CONTENT_AUTHORITY, MovieContract.PATH_MOVIE + "/#", MOVIE_WITH_ID);
        uriMatcher.addURI(MovieContract.CONTENT_AUTHORITY, MovieContract.PATH_MOVIE + "/#/videos", MOVIE_WITH_ID_VIDEOS);
        uriMatcher.addURI(MovieContract.CONTENT_AUTHORITY, MovieContract.PATH_MOVIE + "/popular", MOVIE_LIST_POPULAR);
        uriMatcher.addURI(MovieContract.CONTENT_AUTHORITY, MovieContract.PATH_MOVIE + "/rating", MOVIE_LIST_RATING);
        uriMatcher.addURI(MovieContract.CONTENT_AUTHORITY, MovieContract.PATH_MOVIE + "/favorite", MOVIE_LIST_FAVORITE);
        uriMatcher.addURI(MovieContract.CONTENT_AUTHORITY, MovieContract.PATH_VIDEO, VIDEO);
        uriMatcher.addURI(MovieContract.CONTENT_AUTHORITY, MovieContract.PATH_FAVORITE, FAVORITE);

        // 3) Return the new matcher!
        return uriMatcher;
    }

    /** Get the movie id from the URI and use it to get a proper cursor */
    private Cursor getMovieWithId(Uri uri, String[] projection, String sortOrder) {
        long movieId = MovieContract.MovieEntry.getMovieIdFromUri(uri);
        String selection = sMovieSelection;
//        Log.v(LOG_TAG, "movie selection: " + sMovieSelection);
        String[] selectionArgs = new String[]{Long.toString(movieId)};
        Cursor c = sMovieFavoriteQueryBuilder.query(mOpenHelper.getReadableDatabase(),
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null
        );
//        if (c.moveToFirst()) {
//            Log.v(LOG_TAG, String.format("The 'movie' cursor has %d rows and %d columns", c.getCount(), c.getColumnCount()));
////            while (c.moveToNext()) {
////                Log.v(LOG_TAG, String.format("Data row for movie %s and poster_path %s",c
////                        .getString(0), c.getString(2)));
////            }
//        } else {
//            Log.w(LOG_TAG, "no data returned in the 'movie' cursor");
//        }
        return c;
    }

    /** Get the movie id from the URI and use it to get a proper cursor WITH it's videos */
    private Cursor getMovieWithIdVideos(Uri uri, String[] projection, String sortOrder) {
        long movieId = MovieContract.MovieEntry.getMovieIdFromUri(uri);
        String selection = sVideoSelection;
//        Log.v(LOG_TAG, "video selection: " + sVideoSelection);
        String[] selectionArgs = new String[]{Long.toString(movieId)};
        Cursor c =  sVideoFavoriteQueryBuilder.query(mOpenHelper.getReadableDatabase(),
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );
//        if (c.moveToFirst()) {
//            Log.v(LOG_TAG, String.format("The 'movie Videos' cursor has %d rows and %d columns", c.getCount(), c.getColumnCount()));
////            while (c.moveToNext()) {
////                Log.v(LOG_TAG, String.format("Data row for movie %s and poster_path %s",c
////                        .getString(0), c.getString(2)));
////            }
//        } else {
//            Log.w(LOG_TAG, "no data returned in the 'movie Videos' cursor");
//        }
        return c;
    }

    private Cursor getMoviePopular (Uri uri, String[] projection, String sortOrder) {
        Cursor c = sMovieFavoriteQueryBuilder.query(mOpenHelper.getReadableDatabase(),
                projection,
                null,
                null,
                null,
                null,
                MovieContract.MovieEntry.TABLE_NAME + "." +
                        MovieContract.MovieEntry.COLUMN_POPULARITY + " desc"
        );
//        if (c.moveToFirst()) {
//            Log.v(LOG_TAG, String.format("The 'popular movies' cursor has %d rows and %d columns", c.getCount(), c.getColumnCount()));
//            while (c.moveToNext()) {
//                Log.v(LOG_TAG, String.format("Data row for movie %s and poster_path %s",c
//                        .getString(0), c.getString(2)));
//            }
//        } else {
//            Log.w(LOG_TAG, "no data returned in the cursor");
//        }
        return c;
    }

    private Cursor getMovieRating (Uri uri, String[] projection, String sortOrder) {
        return sMovieFavoriteQueryBuilder.query(mOpenHelper.getReadableDatabase(),
                projection,
                null,
                null,
                null,
                null,
                MovieContract.MovieEntry.TABLE_NAME + "." +
                        MovieContract.MovieEntry.COLUMN_VOTE_AVERAGE + " desc, " +
                        MovieContract.MovieEntry.TABLE_NAME + "." +
                        MovieContract.MovieEntry.COLUMN_VOTE_COUNT + " desc"
        );
    }

    private Cursor getMovieFavorites (Uri uri, String[] projection, String sortOrder) {
//        Log.v(LOG_TAG, "trying to get the favorite movies!");
        String selection = sMovieListFavorites;
        Cursor c = sMovieFavoriteQueryBuilder.query(mOpenHelper.getReadableDatabase(),
                projection,
                selection,
                null,
                null,
                null,
                MovieContract.FavoriteEntry.TABLE_NAME + "." + MovieContract.FavoriteEntry._ID + " DESC"
        );
//        if (c.moveToFirst()) {
//            Log.v(LOG_TAG, String.format("The 'favorites movies' cursor has %d rows and %d columns", c.getCount(), c.getColumnCount()));
//            while (c.moveToNext()) {
//                Log.v(LOG_TAG, String.format("Data row for movie %s and poster_path %s",c
//                        .getString(0), c.getString(2)));
//            }
//        } else {
//            Log.w(LOG_TAG, "no data returned in the favorite cursor");
//        }
        return c;
    }
}

