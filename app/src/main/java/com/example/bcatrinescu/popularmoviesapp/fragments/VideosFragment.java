package com.example.bcatrinescu.popularmoviesapp.fragments;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bcatrinescu.popularmoviesapp.NonScrollListView;
import com.example.bcatrinescu.popularmoviesapp.R;
import com.example.bcatrinescu.popularmoviesapp.adapters.VideosCursorAdapter;
import com.example.bcatrinescu.popularmoviesapp.data.MovieContract;

public class VideosFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final String LOG_TAG = VideosFragment.class.getSimpleName();
    public static final String VIDEOSFRAGMENT_TAG = "VFTAG";
    private static final int VIDEO_LOADER = 511562;
    private VideosCursorAdapter mVideosCursorAdapter;
    private Uri mUri;

    /**
     *  It seems that the Cursor loader NEEDS an _ID column no matter what...
     *  so make sure you include it in your query projection!
     *  */
    public static final String[] VIDEOS_COLUMNS = {
            MovieContract.VideosEntry.TABLE_NAME + "." + MovieContract.VideosEntry._ID,
            MovieContract.VideosEntry.COLUMN_MOVIE_ID,
            MovieContract.VideosEntry.COLUMN_TYPE,
            MovieContract.VideosEntry.COLUMN_NAME,
            MovieContract.VideosEntry.COLUMN_LANGUAGE,
            MovieContract.VideosEntry.COLUMN_SITE,
            MovieContract.VideosEntry.COLUMN_KEY,
            MovieContract.VideosEntry.COLUMN_RES_SIZE,
    };

    // These indices are tied to the VIDEOS_COLUMNS. If VIDEOS_COLUMNS changes, the values below MUST change
    public static final int COL_VID_ID = 0;
    public static final int COL_VID_MOVIE_ID = 1;
    public static final int COL_VID_TYPE = 2;
    public static final int COL_VID_NAME = 3;
    public static final int COL_VID_LANGUAGE = 4;
    public static final int COL_VID_SITE = 5;
    public static final int COL_VID_KEY = 6;
    public static final int COL_VID_RES_SIZE = 7;


    public static Fragment getInstanceByUri(Uri uri) {
        VideosFragment videosFragment = null;
        if (uri != null) {
            videosFragment = new VideosFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable(VIDEOSFRAGMENT_TAG, uri);
            videosFragment.setArguments(bundle);
        } else {
            Log.w(LOG_TAG, "The Uri passed by the parent activity is NULL. I can't create a Fragment instance");
        }
        return videosFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mUri = arguments.getParcelable(VideosFragment.VIDEOSFRAGMENT_TAG);
        } else {
            Log.w(LOG_TAG, "The arguments (parcelable) seem to be null. Did the origin activity set this correctly?");
        }
        mVideosCursorAdapter = new VideosCursorAdapter(getContext(), null, 0);
        View view = inflater.inflate(R.layout.fragment_videos, container, false);
        NonScrollListView videoList = (NonScrollListView) view.findViewById(R.id.videos_list);
        videoList.setAdapter(mVideosCursorAdapter);
        videoList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = (Cursor) parent.getItemAtPosition(position);
                String videoKey = cursor.getString(COL_VID_KEY);
                Intent openVideoIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + Uri.encode(videoKey)));
                if (openVideoIntent.resolveActivity(getContext().getPackageManager()) != null) {
                    // send that intent then!
                    getContext().startActivity(openVideoIntent);
                } else {
                    // inform the user that there is no Youtube application
                    Toast.makeText(getContext(), "No \"Video/Youtube\" application found! Opening in browser", Toast.LENGTH_SHORT).show();
                    Intent openVideoBrowserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=" + Uri.encode(videoKey)));
                    if (openVideoBrowserIntent.resolveActivity(getContext().getPackageManager()) != null) {
                        // send that intent then!
                        getContext().startActivity(openVideoBrowserIntent);
                    } else {
                        Log.w(LOG_TAG, "No app found that could handle the Uri: " + Uri.parse("http://www.youtube.com/watch?v=" + Uri.encode(videoKey)));
                    }
                }
            }
        });
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getLoaderManager().restartLoader(VIDEO_LOADER, null, this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        getLoaderManager().initLoader(VIDEO_LOADER, null, this);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(
                getContext(),
                mUri, // the Uri will be created and send this from the MoviesActivity callback
                VIDEOS_COLUMNS,
                null,
                null,
                null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data.moveToFirst()) {
//            Log.v(LOG_TAG, String.format("The 'movie video' cursor has %d rows and %d columns",
//                    data.getCount(),
//                    data.getColumnCount()));
//            while (c.moveToNext()) {
//                Log.v(LOG_TAG, String.format("Data row for movie %s and poster_path %s",c
//                        .getString(0), c.getString(2)));
//            }
        } else {
            Log.w(LOG_TAG, "no data returned in the 'movie video' cursor");
        }
        mVideosCursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mVideosCursorAdapter.swapCursor(null);
    }
}
