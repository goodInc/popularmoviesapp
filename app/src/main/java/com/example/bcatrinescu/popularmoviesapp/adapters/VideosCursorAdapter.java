package com.example.bcatrinescu.popularmoviesapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bcatrinescu.popularmoviesapp.R;
import com.example.bcatrinescu.popularmoviesapp.fragments.VideosFragment;

/**
 * Created by bcatrinescu on 04.11.2016.
 */

public class VideosCursorAdapter extends CursorAdapter{
    private static final String LOG_TAG = VideosCursorAdapter.class.getSimpleName();

    public VideosCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View v = LayoutInflater.from(context).inflate(R.layout.fragment_videos_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v, context);
        v.setTag(viewHolder); // set the tag - will be used in the bindView method
        return v;
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        ViewHolder viewHolder = (ViewHolder) view.getTag();

        (viewHolder.videoTypeTextView).setText(cursor.getString(VideosFragment.COL_VID_TYPE));
        (viewHolder.videoNameTextView).setText(cursor.getString(VideosFragment.COL_VID_NAME));
        (viewHolder.videoLanguageTextView).setText(cursor.getString(VideosFragment.COL_VID_LANGUAGE));
        (viewHolder.videoResolutionTextView).setText(cursor.getString(VideosFragment.COL_VID_RES_SIZE));
    }

    /** This static object will store the IDs of each of the available views. Use it in a view TAG */
    private static class ViewHolder {
        public final TextView videoTypeTextView;
        public final TextView videoNameTextView;
        public final TextView videoLanguageTextView;
        public final TextView videoResolutionTextView;

        public ViewHolder(View v, Context context){
            videoTypeTextView = (TextView) v.findViewById(R.id.video_type);
            videoNameTextView = (TextView) v.findViewById(R.id.video_name);
            videoLanguageTextView = (TextView) v.findViewById(R.id.video_language);
            videoResolutionTextView = (TextView) v.findViewById(R.id.video_resolution);
        }
    }
}
