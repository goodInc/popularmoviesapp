package com.example.bcatrinescu.popularmoviesapp.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SyncRequest;
import android.content.SyncResult;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.bcatrinescu.popularmoviesapp.BuildConfig;
import com.example.bcatrinescu.popularmoviesapp.R;
import com.example.bcatrinescu.popularmoviesapp.data.MovieContract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Vector;

/**
 * Created by bcatrinescu on 31.10.2016.
 */
public class MovieSyncAdapter extends AbstractThreadedSyncAdapter {
    private final static String LOG_TAG = MovieSyncAdapter.class.getSimpleName();
    // Interval at which to sync with the weather, in seconds = 60 seconds * 180 = 3 hours
    public static final int SYNC_INTERVAL = 60 * 180;
    // the sync could be done up to 1 hour BEFORE the time is up
    public static final int SYNC_FLEXTIME = SYNC_INTERVAL/3;
    private static String mCurrentSortOrder;
    private final String MOVIE_BASE_URL = "http://api.themoviedb.org/3/movie";
    private final int URL_CONNECT_TIMEOUT_MILS = 2000;
    private final int URL_READ_TIMEOUT_MILS = 2500;
    private static SharedPreferences mSharedPreferences;

    public MovieSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mCurrentSortOrder = mSharedPreferences.getString("sortOrder", "popular");
    }

    /**
     * Helper method to get the fake account to be used with SyncAdapter, or make a new one
     * if the fake account doesn't exist yet.  If we make a new account, we call the
     * onAccountCreated method so we can initialize things.
     *
     * @param context The context used to access the account service
     * @return a fake account.
     */
    public static Account getSyncAccount(Context context) {
        // Get an instance of the Android account manager
        AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);

        // Create the account type and default account
        Account newAccount = new Account(
                context.getString(R.string.app_name), context.getString(R.string.sync_account_type));

        // If the password doesn't exist, the account doesn't exist
        if ( null == accountManager.getPassword(newAccount) ) {

        // Add the account and account type, no password or user data If successful, return the Account object, otherwise report an error.
            if (!accountManager.addAccountExplicitly(newAccount, "", null)) {
                Log.d(LOG_TAG, "getting an account failed. returning null...");
                return null;
            }
            // If you don't set android:syncable="true" in your <provider> element in the manifest, then call ContentResolver.setIsSyncable(account, AUTHORITY, 1) here.
            onAccountCreated(newAccount, context);
        }
        return newAccount;
    }

    private static void onAccountCreated(Account newAccount, Context context) {
        // Since we've created an account
        MovieSyncAdapter.configurePeriodicSync(context, SYNC_INTERVAL, SYNC_FLEXTIME);
        // Without calling setSyncAutomatically, our periodic sync will not be enabled.
        ContentResolver.setSyncAutomatically(newAccount, context.getString(R.string.content_authority), true);
        // Finally, let's do a sync to get things started
        syncImmediately(context);
    }

    /** Helper method to schedule the sync adapter periodic execution */
    public static void configurePeriodicSync(Context context, int syncInterval, int flexTime) {
        Account account = getSyncAccount(context);
        String authority = context.getString(R.string.content_authority);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // we can enable inexact timers in our periodic sync
            SyncRequest request = new SyncRequest.Builder().
                    syncPeriodic(syncInterval, flexTime).
                    setSyncAdapter(account, authority).
                    setExtras(new Bundle()).build();
            Log.v(LOG_TAG, String.format("Configuring periodic sync at %d and flex time at %d",syncInterval, flexTime));
            ContentResolver.requestSync(request);
        } else {
            ContentResolver.addPeriodicSync(account,
                    authority, new Bundle(), syncInterval);
            Log.v(LOG_TAG, String.format("Configuring periodic sync at %d without flex time",syncInterval));
        }
    }

    /**
     * Helper method to have the sync adapter sync immediately
     * @param context The context used to access the account service
     */
    public static void syncImmediately(Context context) {
//        Log.v(LOG_TAG, "MovieSyncAdapter syncImmediately");
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mCurrentSortOrder = mSharedPreferences.getString("sortOrder", "popular");
        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        ContentResolver.requestSync(getSyncAccount(context), context.getString(R.string.content_authority), bundle);
//        Log.v(LOG_TAG, "requestSync called... you should see the onPerformSync method running... any time now...");
    }

    public static void initializeSyncAdapter(Context context) {
        getSyncAccount(context);
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
//        Log.v(LOG_TAG, "++++++onPerformSync is alive!!!!!!!");

        if (mCurrentSortOrder.compareTo("favorite") == 0) {
            Log.w(LOG_TAG, "Favorites selection. No online sync!");
            return;
        }
        // retrieve the movie raw data (JSON) from the movie database
        String moviesRawJsonStr = getMoviesRawJson();
        Vector<ContentValues> prettyMoviesJsonVector  = moviesRawJsonToVector(moviesRawJsonStr);
//        Log.v(LOG_TAG, "Pretty JSON MOVIE data:\n" + moviesRawJsonStr);

        if (prettyMoviesJsonVector == null) {
            Log.e(LOG_TAG, String.format("The MOVIE Vector is null for movie. Maybe you are requesting data too fast and the server is refusing you?"));
            Log.e(LOG_TAG, "SKIP the whole syncronize process...");
            return;
        }
        // store the movie data into the database
        ContentValues[] movieContentValuesAsArray = new ContentValues[prettyMoviesJsonVector.size()];
        provider.getLocalContentProvider().bulkInsert(MovieContract.MovieEntry.CONTENT_URI, prettyMoviesJsonVector.toArray(movieContentValuesAsArray));

        for (int currentMoviePosition = 0; currentMoviePosition < prettyMoviesJsonVector.size(); currentMoviePosition++){
            // first, we must find out for which movie ID we want the video information
            String currentMovieId = prettyMoviesJsonVector.get(currentMoviePosition).getAsString(MovieContract.MovieEntry._ID);
            // now we can retrieve the raw video information and put it in a Vector
            String videosRawJsonStr = getVideosRawJson(currentMovieId);
            Vector<ContentValues> prettyVideosJsonVector  = videosRawJsonToVector(videosRawJsonStr);
//            Log.v(LOG_TAG, "Pretty JSON VIDEO data for movie: " + currentMovieId + "\n" + videosRawJsonStr);
            if (prettyVideosJsonVector == null) {
                Log.e(LOG_TAG, String.format("The Video Vector is null for movie %s and raw data: %s ", currentMovieId, videosRawJsonStr));
                Log.e(LOG_TAG, "SKIP this movie...");
                continue;
            }
            // before we store the new video information in the DB, we must clean it (as videos may be added, removed, or changed)
            int videosCleanedUp = provider.getLocalContentProvider().delete(
                    MovieContract.VideosEntry.CONTENT_URI,
                    MovieContract.VideosEntry.COLUMN_MOVIE_ID + " = ?",
                    new String[]{currentMovieId});
//            Log.v(LOG_TAG, String.format("Cleaned up %d videos before insert", videosCleanedUp));

            // store the video data into the database
            ContentValues[] videoContentValuesAsArray = new ContentValues[prettyVideosJsonVector.size()];
            provider.getLocalContentProvider().bulkInsert(MovieContract.VideosEntry.CONTENT_URI, prettyVideosJsonVector.toArray(videoContentValuesAsArray));
        }

//        Log.v(LOG_TAG, "++++++onPerformSync is done!");
    }

    private String getMoviesRawJson(){
        Uri moviesUri = Uri.parse(MOVIE_BASE_URL).buildUpon().
                appendPath(mCurrentSortOrder).
                appendQueryParameter("page", "1").appendQueryParameter("api_key", BuildConfig.THE_MOVIE_DATABASE_API_KEY).
                build();
//        Log.v(LOG_TAG, "The Movies URL to query is: " + moviesUri.toString());

        HttpURLConnection httpURLConnection = null;
        BufferedReader bufferedReader = null;
        StringBuilder resultStringBuilder = new StringBuilder(); // the raw retrieved data will be put here (JSON)
        URL url = null;

        try {
            url = new URL(moviesUri.toString());
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setReadTimeout(URL_READ_TIMEOUT_MILS);
            httpURLConnection.setConnectTimeout(URL_CONNECT_TIMEOUT_MILS);
            httpURLConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = httpURLConnection.getInputStream();
            // If the inputStream variable is empty, there is nothing to do. No data was fetched
            if (inputStream == null) {
                Log.w(LOG_TAG, "For some reason, the 'http input stream' is null");
                return null;
            }
            // if there is data in the inputStream, continue making a buffered reader
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                // since this is a JSON, newlines don't really count, but it will make a pretty output
                // useful for debugging
                resultStringBuilder.append(line + "\n");
            }
            if (resultStringBuilder.length() == 0) {
                Log.w(LOG_TAG, "result string is empty, no point in continuing");
                return null;
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "URL creation from Uri seems to be broken. The Uri is: " + moviesUri.toString());
            e.printStackTrace();
        } catch (IOException e) {
            Log.e(LOG_TAG, "Can't open the HTTP connection (not yet connected). The URL is: " + url.toString());
            e.printStackTrace();
        } finally {
            if (httpURLConnection != null) httpURLConnection.disconnect();
            if (bufferedReader != null) {
                try { bufferedReader.close(); } catch (IOException e) {
                    Log.e(LOG_TAG, "Error closing buffered reader stream", e);
                }
            }
        }
        return resultStringBuilder.toString();
    }

    private String getVideosRawJson(String movie_id){
        Uri videosUri = Uri.parse(MOVIE_BASE_URL).buildUpon().
                appendPath(movie_id).appendPath("videos").
                appendQueryParameter("api_key", BuildConfig.THE_MOVIE_DATABASE_API_KEY).
                build();
//        Log.v(LOG_TAG, "The Video URL to query is: " + videosUri.toString());

        HttpURLConnection httpURLConnection = null;
        BufferedReader bufferedReader = null;
        StringBuilder resultStringBuilder = new StringBuilder(); // the raw retrieved data will be put here (JSON)
        URL url = null;

        try {
            url = new URL(videosUri.toString());
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setReadTimeout(URL_READ_TIMEOUT_MILS);
            httpURLConnection.setConnectTimeout(URL_CONNECT_TIMEOUT_MILS);
            httpURLConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = httpURLConnection.getInputStream();
            // If the inputStream variable is empty, there is nothing to do. No data was fetched
            if (inputStream == null) {
                Log.w(LOG_TAG, "For some reason, the 'http input stream' is null");
                return null;
            }
            // if there is data in the inputStream, continue making a buffered reader
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                // since this is a JSON, newlines don't really count, but it will make a pretty output
                // useful for debugging
                resultStringBuilder.append(line + "\n");
            }
            if (resultStringBuilder.length() == 0) {
                Log.w(LOG_TAG, "result string is empty, no point in continuing");
                return null;
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "URL creation from Uri seems to be broken. The Uri is: " + videosUri.toString());
            e.printStackTrace();
        } catch (IOException e) {
            Log.e(LOG_TAG, "Can't open the HTTP connection (not yet connected). The URL is: " + url.toString());
            e.printStackTrace();
        } finally {
            if (httpURLConnection != null) httpURLConnection.disconnect();
            if (bufferedReader != null) {
                try { bufferedReader.close(); } catch (IOException e) {
                    Log.e(LOG_TAG, "Error closing buffered reader stream", e);
                }
            }
        }
        return resultStringBuilder.toString();
    }

    private Vector<ContentValues> moviesRawJsonToVector(String moviesRawJson){
        Vector<ContentValues> movieContentValuesVector = null;
        try {
            JSONObject moviesRawJsonAsObject = new JSONObject(moviesRawJson);
            JSONArray moviesJsonArray = moviesRawJsonAsObject.getJSONArray("results");
            int numberOfMovies = moviesJsonArray.length();
            movieContentValuesVector = new Vector<>(moviesJsonArray.length());

            for (int currentMoviePosInArray = 0; currentMoviePosInArray < numberOfMovies; currentMoviePosInArray++) {
                JSONObject movieJsonObject = moviesJsonArray.optJSONObject(currentMoviePosInArray);

                ContentValues aMovieValues = new ContentValues();

                String movie_id = movieJsonObject.getString("id");
                String originalTitle = movieJsonObject.getString("original_title");
                String posterPath = movieJsonObject.getString("poster_path").substring(1);
                String overview = movieJsonObject.getString("overview");
                String voteAverage = movieJsonObject.getString("vote_average");
                String voteCount = movieJsonObject.getString("vote_count");
                String popularity = movieJsonObject.getString("popularity");
                String releaseDate = movieJsonObject.getString("release_date");


                aMovieValues.put(MovieContract.MovieEntry._ID, movie_id);
                aMovieValues.put(MovieContract.MovieEntry.COLUMN_ORIGINAL_TITLE, originalTitle);
                aMovieValues.put(MovieContract.MovieEntry.COLUMN_POSTER_PATH, posterPath);
                aMovieValues.put(MovieContract.MovieEntry.COLUMN_OVERVIEW, overview);
                aMovieValues.put(MovieContract.MovieEntry.COLUMN_VOTE_AVERAGE, voteAverage);
                aMovieValues.put(MovieContract.MovieEntry.COLUMN_VOTE_COUNT, voteCount);
                aMovieValues.put(MovieContract.MovieEntry.COLUMN_POPULARITY, popularity);
                aMovieValues.put(MovieContract.MovieEntry.COLUMN_RELEASE_DATE, releaseDate);

                // add the movie information in our Vector, which, at the end, will contain
                // the information about all the movies in the retrieved page (as the movie API data is send over in separate pages)
                movieContentValuesVector.add(aMovieValues);
            }
        } catch (JSONException e) {
            // If you reached this point, something went wrong while extracting the MOVIE information from the JSON
            Log.w(LOG_TAG, "something went wrong while extracting the MOVIE information from the JSON. Watch the StackTrace");
            e.printStackTrace();
        }
        // now that we have all the movies for the given page put in this Vector, just return it
        return movieContentValuesVector;
    }

    private Vector<ContentValues> videosRawJsonToVector(String videosRawJson){
        Vector<ContentValues> videoContentValuesVector = null;
        try {
            JSONObject videosRawJsonAsObject = new JSONObject(videosRawJson);
            String movie_id = videosRawJsonAsObject.getString("id");
            JSONArray videosJsonArray = videosRawJsonAsObject.getJSONArray("results");
            int numberOfVideos = videosJsonArray.length();
            videoContentValuesVector = new Vector<>(numberOfVideos);

            for (int currentVideoPosInArray = 0; currentVideoPosInArray < numberOfVideos; currentVideoPosInArray++) {
                JSONObject aVideoAsJsonObject = videosJsonArray.getJSONObject(currentVideoPosInArray);

                ContentValues aVideoValues = new ContentValues();

                String vid_type = aVideoAsJsonObject.getString("type");
                String vid_name = aVideoAsJsonObject.getString("name");
                String vid_lang = aVideoAsJsonObject.getString("iso_639_1");
                String vid_site = aVideoAsJsonObject.getString("site");
                String vid_key = aVideoAsJsonObject.getString("key");
                String res_size = aVideoAsJsonObject.getString("size");

                aVideoValues.put(MovieContract.VideosEntry.COLUMN_MOVIE_ID, movie_id);
                aVideoValues.put(MovieContract.VideosEntry.COLUMN_TYPE, vid_type);
                aVideoValues.put(MovieContract.VideosEntry.COLUMN_NAME, vid_name);
                aVideoValues.put(MovieContract.VideosEntry.COLUMN_LANGUAGE, vid_lang);
                aVideoValues.put(MovieContract.VideosEntry.COLUMN_SITE, vid_site);
                aVideoValues.put(MovieContract.VideosEntry.COLUMN_KEY, vid_key);
                aVideoValues.put(MovieContract.VideosEntry.COLUMN_RES_SIZE, res_size);

                // add the video information in our Vector, which, at the end, will contain
                // the information about all the videos for a given movie
                videoContentValuesVector.add(aVideoValues);
            }

        } catch (JSONException e) {
            // If you reached this point, something went wrong while extracting the VIDEO information from the JSON
            Log.w(LOG_TAG, "something went wrong while extracting the VIDEO information from the JSON. Watch the StackTrace");
            e.printStackTrace();
        }
        // now that we have all the videos for the given movie put in this Vector, just return it
        return videoContentValuesVector;
    }
}
