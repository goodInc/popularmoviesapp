package com.example.bcatrinescu.popularmoviesapp.adapters;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bcatrinescu.popularmoviesapp.BuildConfig;
import com.example.bcatrinescu.popularmoviesapp.R;
import com.example.bcatrinescu.popularmoviesapp.data.MovieContract;
import com.example.bcatrinescu.popularmoviesapp.fragments.MoviesFragment;
import com.squareup.picasso.Picasso;

import static com.example.bcatrinescu.popularmoviesapp.fragments.MoviesFragment.COL_FAV_MOVIE_ID;
import static com.example.bcatrinescu.popularmoviesapp.fragments.MoviesFragment.COL_MOVIE_ID;
import static com.example.bcatrinescu.popularmoviesapp.fragments.MoviesFragment.COL_VOTE_AVERAGE;

/**
 * Created by bcatrinescu on 02.11.2016.
 */

public class MoviesCursorAdapter extends CursorAdapter {
    private static final String LOG_TAG = MoviesCursorAdapter.class.getSimpleName();
    private static final String POSTERSIZESMALL = "w154";
    private static final String POSTERSIZELARGE = "w342";
    private static final String MOVIE_BASE_URL = "http://image.tmdb.org/t/p";
    private static final int VIEW_TYPE_MOVIE = 0;

    public MoviesCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        // Choose layout type
        int viewType = getItemViewType(cursor.getPosition());
        int layoutId = -1;
        // You can switch from multiple layouts here. However, we just have one layout, for now,
        // so we will assign the value directly
        switch (viewType) {
            case VIEW_TYPE_MOVIE: {
                layoutId = R.layout.fragment_movies_item;
                break;
            }
        }
        View v = LayoutInflater.from(context).inflate(layoutId, parent, false);
        ViewHolder viewHolder = new ViewHolder(v, context);
        v.setTag(viewHolder);
//        Log.v(LOG_TAG, "new view of type: " + VIEW_TYPE_MOVIE);
        return v;
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        /** Get the holder object containing each View id we are interested in, part of our layout */
        final ViewHolder viewHolder = (ViewHolder) view.getTag();

        String moviePoster = cursor.getString(MoviesFragment.COL_POSTER_PATH);
        Uri posterUri = Uri.parse(MOVIE_BASE_URL).buildUpon().appendPath(POSTERSIZESMALL).appendPath(moviePoster).appendQueryParameter("api_key", BuildConfig.THE_MOVIE_DATABASE_API_KEY).build();

        // Load the poster
        Picasso.with(context).load(posterUri).into(viewHolder.moviePosterImageView);

        // Now, show the rating of this movie on the bottom left hand side of the poster
        (viewHolder.ratingTextView).setText(cursor.getString(COL_VOTE_AVERAGE));

        // Inform the user if this is one of it's favorite movies by placing a golden star on the bottom right
        // if it's not a favorite movie, the star should be gray
        if (cursor.getString(COL_FAV_MOVIE_ID) == null) {
            (viewHolder.favoriteButton).setImageResource(android.R.drawable.btn_star_big_off);
            (viewHolder.favoriteButton).setImageAlpha(150);
        } else {
            (viewHolder.favoriteButton).setImageResource(android.R.drawable.btn_star_big_on);
            (viewHolder.favoriteButton).setImageAlpha(250);
        }

        (viewHolder.favoriteButton).setTag(R.string.cursor_movie_id_tag, cursor.getString(COL_MOVIE_ID));
        (viewHolder.favoriteButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentResolver contentResolver = context.getContentResolver();
                String movieId = (String) v.getTag(R.string.cursor_movie_id_tag);

                ContentValues contentValues = new ContentValues();
                contentValues.put(MovieContract.FavoriteEntry.COLUMN_FAVORITE_MOVIE_ID, movieId);

                // A cursor is your primary interface to the query results.
                Cursor cursor = contentResolver.query(
                        MovieContract.FavoriteEntry.CONTENT_URI,
                        null,
                        MovieContract.FavoriteEntry.COLUMN_FAVORITE_MOVIE_ID + "=?",
                        new String[]{movieId},
                        null);

                if (cursor.moveToFirst() == false) {
                    Uri uri = contentResolver.insert(MovieContract.FavoriteEntry.CONTENT_URI, contentValues);
                    (viewHolder.favoriteButton).setImageResource(android.R.drawable.btn_star_big_on);
                    (viewHolder.favoriteButton).setImageAlpha(250);
//                    Log.v(LOG_TAG, "+++++ adding a favorite movie " + movieId + " " + uri);
                } else {
                    (viewHolder.favoriteButton).setImageResource(android.R.drawable.btn_star_big_off);
                    (viewHolder.favoriteButton).setImageAlpha(150);
                    int rowsDeleted = contentResolver.delete(MovieContract.FavoriteEntry.CONTENT_URI, MovieContract.FavoriteEntry.COLUMN_FAVORITE_MOVIE_ID + " <= ?", new String[]{movieId});
//                    Log.v(LOG_TAG, "+++++ deleting the existing favorite movie " + movieId + ". Rows deleted: " + rowsDeleted);
                }
            }
        });
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return super.getView(position, convertView, parent);
    }

    @Override
    public int getViewTypeCount() {
        // we only have 1 layout so... just return 1
        return 1;
    }
    @Override
    public int getItemViewType(int position) {
        // we only have 1 layout so... the decision below will translate to only one view type
//        Log.v(LOG_TAG, "position is " + position);
        return (position == 0) ? VIEW_TYPE_MOVIE : VIEW_TYPE_MOVIE;
    }

    /** This static object will store the IDs of each of the available views. Use it in a view TAG */
    private class ViewHolder {
        public final ImageView moviePosterImageView;
        public final ImageButton favoriteButton;
        public final TextView ratingTextView;

        public ViewHolder(View v, Context context){
            moviePosterImageView = (ImageView) v.findViewById(R.id.movie_poster);
            favoriteButton = (ImageButton) v.findViewById(R.id.favoriteButton);
            ratingTextView = (TextView) v.findViewById(R.id.movie_poster_rating_text_view);
        }
    }
}
