package com.example.bcatrinescu.popularmoviesapp.data;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

/**
 * Created by bcatrinescu on 28.10.2016.
 */

public class MovieContract {
    private static final String LOG_TAG = MovieContract.class.getSimpleName();
    // The "Content authority" is a name for the entire content provider, similar to the
    // relationship between a domain name and its website.  A convenient string to use for the
    // content authority is the package name for the app, which is guaranteed to be unique on the
    // device.
    public static final String CONTENT_AUTHORITY = "com.example.bcatrinescu.popularmoviesapp";
    // Use CONTENT_AUTHORITY to create the base of all URI's which apps will use to contact
    // the content provider.
    private static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    // Possible paths (appended to base content URI for possible URI's)
    // For instance, content://com.example.android.sunshine.app/weather/ is a valid path for
    // looking at weather data. content://com.example.android.sunshine.app/givemeroot/ will fail,
    // as the ContentProvider hasn't been given any information on what to do with "givemeroot".
    // At least, let's hope not.  Don't be that dev, reader.  Don't be that dev.
    public static final String PATH_MOVIE = "movie"; // list of movies
    public static final String PATH_VIDEO = "video"; // movie associated videos (e.g. trailers)
    public static final String PATH_FAVORITE = "favorite"; // favorite movies

    /** Inner class that defines the "movie" table */
    public static final class MovieEntry implements BaseColumns {
        public static final String TABLE_NAME = "movie";
        // define all the necessary columns
        public static final String COLUMN_ORIGINAL_TITLE = "original_title";
        public static final String COLUMN_POSTER_PATH = "poster_path";
        public static final String COLUMN_OVERVIEW = "overview";
        public static final String COLUMN_VOTE_AVERAGE = "vote_average";
        public static final String COLUMN_VOTE_COUNT = "vote_count";
        public static final String COLUMN_POPULARITY = "popularity";
        public static final String COLUMN_RELEASE_DATE = "release_date";

        // define the content URI
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_MOVIE).build();
        // define the content type (multi and multi)
        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MOVIE;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MOVIE;

        public static long getMovieIdFromUri(Uri uri) {
//            Log.v(LOG_TAG, uri.toString() + " path is: " + uri.getPathSegments().get(1));
            return Long.parseLong(uri.getPathSegments().get(1));
        }
        public static Uri buildMovieUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static Uri buildPopularMovieUri() {
            return CONTENT_URI.buildUpon().appendPath("popular").build();
        }
        public static Uri buildRatingMovieUri() {
            return CONTENT_URI.buildUpon().appendPath("rating").build();
        }
        public static Uri buildFavoriteMovieUri() {
            return CONTENT_URI.buildUpon().appendPath("favorite").build();
        }
    }

    /** Inner class that defines the "videos" table */
    public static final class VideosEntry implements BaseColumns {
        public static final String TABLE_NAME = "video";
        // define all necessary columns
        public static final String COLUMN_MOVIE_ID = "movie_id"; // foreign key used to join this table with "movie" table
        public static final String COLUMN_TYPE = "vid_type"; // e.g. "trailer" or "featurette"
        public static final String COLUMN_NAME = "vid_name";
        public static final String COLUMN_LANGUAGE = "vid_lang"; // e.g. en
        public static final String COLUMN_SITE = "vid_site"; // e.g. YouTube
        public static final String COLUMN_KEY = "vid_key"; // e.g. "WawU4ouldxU" - the id on YouTube
        public static final String COLUMN_RES_SIZE = "res_size"; // e.g. 720 or 480 - maximum size available

        // define the content URI
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_VIDEO).build();
        // define the content type (multi and multi)
        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_VIDEO;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_VIDEO;

        public static Uri buildVideoUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    /** Inner class that defines the "favorite" table */
    public static final class FavoriteEntry implements BaseColumns {
        public static final String TABLE_NAME = "favorite";
        // define all necessary columns
        public static final String COLUMN_FAVORITE_MOVIE_ID = "favorite_movie_id"; // foreign key used to join this table with "movie" table

        // define the content URI
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_FAVORITE).build();
        // define the content type (multi and multi)
        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_FAVORITE;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_FAVORITE;

        public static Uri buildFavoriteUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }
}
