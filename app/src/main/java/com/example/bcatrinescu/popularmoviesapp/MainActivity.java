package com.example.bcatrinescu.popularmoviesapp;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.bcatrinescu.popularmoviesapp.fragments.MoviesFragment;
import com.example.bcatrinescu.popularmoviesapp.sync.MovieSyncAdapter;

public class MainActivity extends AppCompatActivity implements MoviesFragment.Callback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MovieSyncAdapter.initializeSyncAdapter(this);
    }

    @Override
    public void onItemSelected(Uri movieUri) {
        Intent intent = new Intent(this, MovieDetailsActivity.class).setData(movieUri);
        startActivity(intent);
    }
}
