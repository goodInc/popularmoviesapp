package com.example.bcatrinescu.popularmoviesapp.adapters;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleAdapter;

import com.example.bcatrinescu.popularmoviesapp.BuildConfig;
import com.example.bcatrinescu.popularmoviesapp.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by bcatrinescu on 12.09.2016.
 */
public class MoviesSimpleAdapter extends SimpleAdapter {
    private static final String LOG_TAG = MoviesSimpleAdapter.class.getSimpleName();
    private final String POSTERSIZESMALL = "w154";
    private final String POSTERSIZELARGE = "w342";
    private final String MOVIE_BASE_URL = "http://image.tmdb.org/t/p";
    private static MoviesSimpleAdapter moviesSimpleAdapter;
    private static Context context = null;
    private static List<Map<String, String>> fillData;


    public MoviesSimpleAdapter(Context context, List<Map<String, String>> data, int resource, String[] from, int[] to) {
        super(context, data, resource, from, to);
//        Log.v(LOG_TAG, "MoviesSimpleAdapter constructor call!");
        this.context = context;
    }

    public static MoviesSimpleAdapter getInstance(Context context) {
        if (moviesSimpleAdapter == null) {
            /** define and create the parameter variables needed by the adapter */
            fillData = new ArrayList<>();
            String[] from = createFromMapping();
            int[] to = createToMapping();
            /** create the adapter */
            moviesSimpleAdapter = new MoviesSimpleAdapter(context, fillData, R.layout.fragment_movies_item, from, to);
        }
//        Log.v(LOG_TAG, "MoviesSimpleAdapter GETInstance call call!");
        return moviesSimpleAdapter;
    }

    public static void updateData(List<Map<String, String>> data) {
//        Log.v(LOG_TAG, "MoviesSimpleAdapter updata data call!");
        if (moviesSimpleAdapter == null)
            Log.w(LOG_TAG, "The adapter is NULL");
        fillData.clear();
//        Log.v(LOG_TAG, "updating the ADAPTER with the newest data. Entries: " + data.size());
        fillData.addAll(data);
        moviesSimpleAdapter.notifyDataSetChanged();
    }

    /** for now, we don't need to map any static text to any of the views */
    private static int[] createToMapping() {
        return new int[] {};
    }

    /** for now, we don't need to map any static text to any of the views */
    private static String[] createFromMapping() {
        return new String[] {};
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = LayoutInflater.from(context).inflate(R.layout.fragment_movies_item, parent, false);

        Uri posterUri;
        String posterFileName;
        ImageView posterView;

        posterFileName = (String) ((Map)getItem(position)).get("posterPath");
        posterUri = Uri.parse(MOVIE_BASE_URL).buildUpon().appendPath(POSTERSIZESMALL).appendPath(posterFileName).appendQueryParameter("api_key", BuildConfig.THE_MOVIE_DATABASE_API_KEY).build();
        posterView = (ImageView) v.findViewById(R.id.movie_poster);

        Picasso picasso = Picasso.with(context);
//        picasso.setIndicatorsEnabled(true);
        picasso.load(posterUri).into(posterView);

//        Log.v(LOG_TAG, "The poster for this view is at: " + posterUri.toString());

        return super.getView(position, v, parent);
    }
}
