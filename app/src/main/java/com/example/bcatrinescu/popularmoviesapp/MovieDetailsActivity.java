package com.example.bcatrinescu.popularmoviesapp;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.bcatrinescu.popularmoviesapp.fragments.MovieDetailsFragment;
import com.example.bcatrinescu.popularmoviesapp.fragments.VideosFragment;

public class MovieDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);
        if (savedInstanceState == null) {
            // Create the detail fragment and add it to this activity using a fragment transaction
            Uri movieUri = getIntent().getData(); // get the uri send by the other activity
            Uri videoUri = movieUri.buildUpon().appendPath("videos").build();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.movie_detail_fragment_container, MovieDetailsFragment.getInstanceByUri(movieUri))
                    .add(R.id.movie_detail_fragment_container, VideosFragment.getInstanceByUri(videoUri))
                    .commit();
        }
    }
}
